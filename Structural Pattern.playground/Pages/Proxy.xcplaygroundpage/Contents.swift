//: [Previous](@previous)
/*
 Proxy hides the real object, provides controlled access and usage and custom implementation
 */

import Foundation

/*
 Assume that we need to use an expensive service to save all data.
 */

/*
 Client-facing code
 */

class ClientController {
    public static func load() {
        print("Starting service...")
        let service = ExpensiveExternalService()
        print("Done!")
        do {
            try service.save()
        } catch {
            fatalError("Service not initialized!")
        }
    }
}

ClientController.load()

/*
 Observe that the service took 5 seconds to initialize.
 So we can create a virtual proxy that modify the initializing behaviour of existing real service.
 So we only load the service when needed otherwise we don't initialize it
 By utilizing the lazy property provided by Swift
 */

class VirtualServiceProxy {
    // the lazy keyword make sure this variable is only initialized when it is first needed.
    private lazy var externalService: ExpensiveExternalService = ExpensiveExternalService()
    
    func save() {
        do {
            try externalService.save()
        } catch {
            fatalError("Service not initialized!")
        }
    }
}

// Now we replace the controller's implementation with our virtual proxy instead
extension ClientController {
    public static func loadByProxy() {
        print("Starting service...")
        let service = VirtualServiceProxy()
        print("Done!")
        service.save()
    }
}

print("\n")
ClientController.loadByProxy()

//: [Next](@next)
