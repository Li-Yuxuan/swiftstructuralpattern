import Foundation

public struct ExternalServiceError: Error {}

public protocol ExternalServiceProtocol {
    init()
    func save() throws
}

public struct ExpensiveExternalService: ExternalServiceProtocol {
    private var isServiceInitialized: Bool = false
    public init() {
        sleep(5)
        print("Service initialized!")
        isServiceInitialized = true
    }
    public func save() throws {
        if (isServiceInitialized) {
            print("Saved all objects...")
        } else {
            throw ExternalServiceError()
        }
    }
}
