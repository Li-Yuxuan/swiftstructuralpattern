//: [Previous](@previous)
/*
 * Use Decorator pattern allows us to dynamically extend original object's behaviour/feature with any combinations,
 * Without the needs of subclassing to create static choices. For example, base Macbook have memory add-on and storage add-on and
 * other add-on features, users might want to choose any combination of upgrade so it is unwise to create all the different subclasses
 * and then subclasses them one by one.
 */

import Foundation

enum MemoryTier: Float {
    case _16GB = 300
    case _32GB = 600
    case _64GB = 1200
}

enum StorageTier: Float {
    case _512GBSSD = 300
    case _1TSSD = 600
    case _2TSSD = 1200
    case _4TSSD = 2400
}

protocol MacbookModel: CustomStringConvertible {
    var price: Float { get }
}

class MacbookPro13Inch: MacbookModel {
    var price: Float { return 1300 }
    var description: String { "13-inch Macbook Pro 2022"}
}

class MacbookAir13Inch: MacbookModel {
    var price: Float { return 1050}
    var description: String { "13-inch Macbook Air 2022"}
}

class MacbookPro16Inch: MacbookModel {
    var price: Float { return 2000 }
    var description: String { "16-inch Macbook Pro 2022"}
}

/*
 Use decorator to define the wrapped object for enhancement, and init with wrapped to create recursive effect
 */
protocol MacbookDecorator: MacbookModel {
    var wrapped: MacbookModel { get }
}

extension MacbookDecorator {
    var price: Float { wrapped.price }
    var description: String { wrapped.description }
}

struct MemoryAddOnDecorator: MacbookDecorator {
    var wrapped: MacbookModel
    private var memoryChoice: MemoryTier
    
    init(wrapped: MacbookModel, choice memoryChoice: MemoryTier) {
        self.wrapped = wrapped
        self.memoryChoice = memoryChoice
    }
    
    var price: Float {
        wrapped.price + memoryChoice.rawValue
    }
    
    var description: String {
        switch memoryChoice {
        case ._16GB:
            return wrapped.description + " + 16GB Memory Add-Ons"
        case ._32GB:
            return wrapped.description + " + 32GB Memory Add-Ons"
        case ._64GB:
            return wrapped.description + " + 64GB Memory Add-Ons"
        }
    }
}

struct StorageAddOnDecorator: MacbookDecorator {
    var wrapped: MacbookModel
    private var storageChoice: StorageTier
    
    init(wrapped: MacbookModel, choice storageChoice: StorageTier) {
        self.wrapped = wrapped
        self.storageChoice = storageChoice
    }
    
    var price: Float {
        return wrapped.price + storageChoice.rawValue
    }
    
    var description: String {
        switch storageChoice {
        case ._512GBSSD:
            return wrapped.description + " + 512GB SSD Upgrades"
        case._1TSSD:
            return wrapped.description + " + 1T SSD Upgrades"
        case ._2TSSD:
            return wrapped.description + " + 2T SSD Upgrades"
        case ._4TSSD:
            return wrapped.description + " + 4T SSD Upgrades"
        }
    }
}

/*
 Main Program
 */

var macbookPurchased: MacbookModel = MacbookPro16Inch()
print("\(macbookPurchased)\n\t$\(macbookPurchased.price)")

// add-ons
macbookPurchased = MemoryAddOnDecorator(wrapped: macbookPurchased, choice: ._64GB)
print("\(macbookPurchased)\n\t$\(macbookPurchased.price)")

macbookPurchased = StorageAddOnDecorator(wrapped: macbookPurchased, choice: ._4TSSD)
print("\(macbookPurchased)\n\t$\(macbookPurchased.price)")

//: [Next](@next)
