//: [Previous](@previous)
/*
 We want to unpack boxes of products to analyze the contents aggregated.
 Box contains boxes and products. It is a tree structure downwards.
 We use composite pattern to avoid unwrapping all the boxes to go over all products then calculate total.
 */

import Foundation

/*
 This is the common protocol for container and leaf to conform to
 */
protocol ProductAnalyzer {
    var price: Double { get }
    var name: String { get }
    var quantity: Int { get }
    var level: Int { get }
}

class Box: ProductAnalyzer {
    
    var price: Double {
        items.reduce(0.0, { $0 + $1.price * Double($1.quantity) })
    }
    var name: String { "Box" }
    var quantity: Int {
        items.reduce(0, { $0 + $1.quantity })
    }
    var level: Int { boxLevel }
    
    private var boxLevel: Int
    private var items: [ProductAnalyzer] = []
    
    init(level: Int) {
        self.boxLevel = level
    }
    
    func add(item: ProductAnalyzer) {
        items.append(item)
    }
}

class Product: ProductAnalyzer {
    var price: Double { productPrice }
    var name: String { productName }
    var quantity: Int { productQuantity }
    var level: Int { productLevel }
    
    private var productName: String
    private var productPrice: Double
    private var productQuantity: Int
    private var productLevel: Int
    
    init(name: String, price: Double, quantity: Int, level: Int) {
        self.productName = name
        self.productPrice = price
        self.productQuantity = quantity
        self.productLevel = level
    }
}

extension Box: CustomStringConvertible {
    var description: String {
        var summary = "/Level \(level) Container Box: \(quantity) items inside with total value of $\(round(price * 100)/100)"
        items.forEach { item in
            let indent = String(repeating: "\t", count: level + 1)
            summary += "\n\(indent)\(item)"
        }
        return summary
    }
}

extension Product: CustomStringConvertible {
    var description: String {
        return "- \(productName): $\(productPrice) x \(productQuantity)"
    }
}

let rootBox = Box(level: 0)
let firstLevelBox = Box(level: 1)
let firstLevelProductA = Product(name: "Product A", price: 20.3, quantity: 12, level: 1)
let firstLevelProductB = Product(name: "Product B", price: 13.4, quantity: 24, level: 1)
let secondLevelBox = Box(level: 2)
let secondLevelProductC = Product(name: "Product C", price: 14.8, quantity: 14, level: 2)
let thirdLevelBox = Box(level: 3)

rootBox.add(item: firstLevelBox)
firstLevelBox.add(item: secondLevelBox)
secondLevelBox.add(item: thirdLevelBox)
rootBox.add(item: firstLevelProductA)
rootBox.add(item: firstLevelProductB)
firstLevelBox.add(item: secondLevelProductC)

print(rootBox)
