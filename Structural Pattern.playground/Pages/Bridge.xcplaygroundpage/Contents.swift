//: [Previous](@previous)
/*
 Bridge pattern solve the exploding class hierarchies by splitting
 them into separate parallel hierarchies - abstraction | implementation
 
 Good for different platforms
 */
import Foundation

/*
 We have Windows / MacOS / Ubuntu 3 different platforms
 They all have different implementations for downloading
 */

protocol downloadProtocol {
    func download()
}

/*
 3 systems have different downloading implementations
 */

enum System: String {
    case Windows = "Windows"
    case Mac = "MacOS"
    case Ubuntu = "Ubuntu"
}

class WindowsDownloadImpl: downloadProtocol {
    func download() {
        print("Windows system don't check anything...")
        print("Windows system download...")
    }
}

class MacDownloadImpl: downloadProtocol {
    func download() {
        print("MacOS system security check...")
        print("MacOS system download...")
    }
}

class UbuntuDownloadImpl: downloadProtocol {
    func download() {
        print("Ubuntu system download...")
        print("Ubuntu system scanning files for risk...")
    }
}

/*
 Here is the abstraction that all systems share in common
 */
protocol systemNetworkingProtocol {
    var downloadImpl: downloadProtocol { get }
    func connect(_ ip: String, _ completion: @escaping (Bool) -> Void)
}

class CoreMachine: systemNetworkingProtocol {
    var downloadImpl: downloadProtocol
    
    private var system: System
    
    init(using system: System) {
        self.system = system
        switch system {
        case .Windows:
            downloadImpl = WindowsDownloadImpl()
        case .Mac:
            downloadImpl = MacDownloadImpl()
        case .Ubuntu:
            downloadImpl = UbuntuDownloadImpl()
        }
    }
    
    func connect(_ ip: String, _ completion: @escaping (Bool) -> Void) {
        print("Machine is using \(system) system.")
        print("Connecting to \(ip)...")
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            print("Connected!")
            self.downloadImpl.download()
            completion(true)
        }
    }
    
}

/*
 Main Program
 */

let windowsMachine = CoreMachine(using: .Windows)
let macMachine = CoreMachine(using: .Mac)
let ubuntuMachine = CoreMachine(using: .Ubuntu)

windowsMachine.connect("192.168.0.101") { success in
    if (success) {
        print("Done!")
    }
}

macMachine.connect("10.10.2.17") { success in
    if (success) {
        print("Done!")
    }
}

//: [Next](@next)
