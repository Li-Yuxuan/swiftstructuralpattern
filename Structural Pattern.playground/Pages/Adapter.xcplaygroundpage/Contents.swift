/*
 When we want to introduce the Third Party Messaging Application into our App, and
 we can't modify the Third Party Application's source code. Adapter pattern enables
 us to do so via Swift extension
 */

import Foundation

protocol MessageSharingProtocol {
    @discardableResult
    func postMessage(message: String) -> Bool
}

class MessagingAppA: MessageSharingProtocol {
    func postMessage(message: String) -> Bool {
        debugPrint("A posts the message: \(message)...")
        return true
    }
}

class MessagingAppB: MessageSharingProtocol {
    func postMessage(message: String) -> Bool {
        debugPrint("B posts the message: \(message)...")
        return true
    }
}

/*
 Use extension to allow third party tool conform to our implementation and bridge that similar implementation
 */
extension ThirdPartyMessagingApplication: MessageSharingProtocol {
    func postMessage(message: String) -> Bool {
        self.sendMessage(text: message, completionHandler: nil)
        return true
    }
}

public enum ApplicationType {
    case AppA
    case AppB
    case ThirdPartyApplication
}

public class OurMessageRelayAgent {
    
    private let supportedApplications: [ApplicationType: MessageSharingProtocol] = [.AppA: MessagingAppA(), .AppB: MessagingAppB(), .ThirdPartyApplication: ThirdPartyMessagingApplication()]
    
    public func postMessage(message: String, applicationType: ApplicationType) -> Bool {
        guard let targetApplication = supportedApplications[applicationType] else { return false }
        targetApplication.postMessage(message: message)
        return true
    }
    
    public func broadcastMessage(message: String) {
        supportedApplications.values.forEach { (application) in
            application.postMessage(message: message)
        }
    }
}

/*
 Main Program
 */

let app = OurMessageRelayAgent()
app.broadcastMessage(message: "Hello World!")
