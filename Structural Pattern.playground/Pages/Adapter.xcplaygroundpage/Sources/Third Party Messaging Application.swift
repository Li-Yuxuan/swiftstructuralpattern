import Foundation

public protocol ThirdPartyMessagingProtocol {
    func sendMessage(text: String, completionHandler: ((Error?) -> Void)?)
}

public class ThirdPartyMessagingApplication: ThirdPartyMessagingProtocol {
    
    public init() {}
    
    public func sendMessage(text: String, completionHandler: ((Error?) -> Void)?) {
        print("Message \(text) sent on Third Party Application")
        guard let completionHandler = completionHandler else {
            return
        }
        completionHandler(nil)
    }
}
